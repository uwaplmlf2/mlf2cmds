// Mlf2cmds runs on the float side and interacts with mlf2session on the
// Shore Server to retrieve the list of commands for the float. This
// program must have its standard input and standard output connected to
// the server.
package main

import (
	"context"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/params"
	shell "bitbucket.org/uwaplmlf2/go-shell"
	"github.com/google/shlex"
)

const USAGE = `
mlf2cmds [options] paramfile

Manage the command portion of communications session with the Shore Server
`

var Version = "dev"
var BuildDate = "unknown"

const PROMPT = "msgs?\n"
const DIR_LISTER = "tree -D"

var (
	showvers  = flag.Bool("version", false, "Show program version information and exit")
	inDir     = flag.String("indir", "/data/INBOX", "Incoming file directory")
	outDir    = flag.String("outdir", "/data/OUTGOING", "Outgoing file directory")
	qDir      = flag.String("queue", "/data/OUTBOX", "Outgoing file queue")
	dataDir   = flag.String("data", "/data/ARCHIVE", "Top level data directory")
	echoMode  = flag.Bool("echo", false, "Echo input characters to output")
	timeOut   = flag.Duration("timeout", 0, "Runtime limit, 0=unlimited")
	dirLister = flag.String("lister", DIR_LISTER, "Directory listing program to call")
	fwdFile   = flag.String("fwd", "", "File of commands to foward to the TT8")
	onePtab   = flag.Bool("single", false, "Parameter file uses single table format")
)

type ioPort struct {
	doEcho bool
	rdr    io.Reader
	wtr    io.Writer
}

func newPort(echo bool) *ioPort {
	return &ioPort{
		doEcho: echo,
		rdr:    os.Stdin,
		wtr:    os.Stdout}
}

func (p *ioPort) Read(b []byte) (n int, err error) {
	if p.doEcho {
		// Stdin must be in raw mode for this to work
		for i := 0; i < len(b); i++ {
			_, err = p.rdr.Read(b[i : i+1])
			if err != nil {
				return n, err
			}
			p.wtr.Write(b[i : i+1])
			n++
		}
		return n, err
	} else {
		return p.rdr.Read(b)
	}
}

func (p *ioPort) Write(b []byte) (int, error) {
	return p.wtr.Write(b)
}

type ArgMissing struct {
	cmd string
}

func (e *ArgMissing) Error() string {
	return e.cmd + ": missing argument"
}

func runProg(cmdline string) ([]byte, error) {
	args, err := shlex.Split(cmdline)
	if err != nil {
		return []byte(""), err
	}
	return exec.Command(args[0], args[1:]...).Output()
}

func getParamCmd(pt *params.Ptable) shell.Command {
	return shell.Command(func(ctx context.Context, args ...string) (string, error) {
		if len(args) < 1 {
			return "", &ArgMissing{cmd: "get"}
		}
		p := pt.Lookup(args[0])
		if p.Name == "" {
			return "", fmt.Errorf("Unknown parameter: %q", args[0])
		}
		return fmt.Sprintf("%s=%s", args[0], p.Value), nil
	})
}

func setParamCmd(pt *params.Ptable, f io.Writer) shell.Command {
	return shell.Command(func(ctx context.Context, args ...string) (string, error) {
		if len(args) < 2 {
			return "", &ArgMissing{cmd: "set"}
		}
		p := pt.Lookup(args[0])
		if p.Name == "" {
			return "", fmt.Errorf("Unknown parameter: %q", args[0])
		}
		if f != nil {
			fmt.Fprintf(f, "set %s\n", strings.Join(args, " "))
		}
		p.Value = args[1]
		return fmt.Sprintf("%s=%s", args[0], p.Value), nil
	})
}

func sendFile(ctx context.Context, args ...string) (string, error) {
	if len(args) < 1 {
		return "", &ArgMissing{cmd: "send-file"}
	}
	pathname := filepath.Join(*dataDir, args[0])
	err := os.Link(pathname, filepath.Join(*outDir, args[0]))
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s added to queue", args[0]), nil
}

func delFile(ctx context.Context, args ...string) (string, error) {
	if len(args) < 1 {
		return "", &ArgMissing{cmd: "del"}
	}
	pathname := filepath.Join(*qDir, args[0])
	err := os.Remove(pathname)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s deleted", pathname), nil
}

var exitCode int = 0

const (
	WaitMode int = 2
	Continue int = 4
)

func startWait(ctx context.Context, args ...string) (string, error) {
	exitCode = WaitMode
	return "Waiting on the surface", nil
}

func dirListing(ctx context.Context, args ...string) (string, error) {
	var dir string
	if len(args) < 1 {
		dir = *dataDir
	} else {
		dir = filepath.Join(*dataDir, args[0])
	}

	output, err := runProg(*dirLister + " " + dir)
	if err != nil {
		return "", fmt.Errorf("dir listing: %w", err)
	}

	// Save the listing to a file named 00index in the outgoing directory
	if f, err := os.Create(filepath.Join(*outDir, "00index")); err == nil {
		f.Write(output)
		f.Close()
	} else {
		return "", fmt.Errorf("write listing file: %w", err)
	}
	return fmt.Sprintf("Listing of %q queued", dir), nil
}

func forwardCommand(name string, f io.Writer, ec int) shell.Command {
	return shell.Command(func(ctx context.Context, args ...string) (string, error) {
		if f != nil {
			fmt.Fprintf(f, "%s %s\n", name, strings.Join(args, " "))
		}
		if ec > 0 {
			exitCode = ec
		}
		return "This command will be forwarded to the TT8", nil
	})
}

func loadPtable(filename string, single bool) (params.Ptable, error) {
	contents, err := ioutil.ReadFile(filename)
	if err != nil {
		return params.Ptable{}, fmt.Errorf("read file: %w", err)
	}

	if single {
		var pt params.Ptable
		err = xml.Unmarshal(contents, &pt)
		return pt, err
	}
	var p params.PtableList
	err = xml.Unmarshal(contents, &p)
	return p.Ptable, err
}

func createShell(pt *params.Ptable, f io.Writer) *shell.Shell {
	iop := newPort(*echoMode)
	opts := []shell.Option{shell.ExitOnEmpty(true), shell.Strict(true)}
	s := shell.New(iop, "", opts...)
	s.AddCommand("get", getParamCmd(pt))
	s.AddCommand("set", setParamCmd(pt, f))
	s.AddCommand("send-file", sendFile)
	s.AddCommand("send-file-next", sendFile)
	s.AddCommand("del", delFile)
	s.AddCommand("dir", dirListing)

	// Forward command to TT8 and change the exit status to WaitMode
	s.AddCommand("wait", forwardCommand("wait", f, WaitMode))

	// Forward command to TT8 and leave the exit status unchanged
	for _, name := range []string{"flasher-on", "flasher-off", "safe", "dump", "release"} {
		s.AddCommand(name, forwardCommand(name, f, -1))
	}

	// Forward command to TT8 and change the exit status to Continue
	for _, name := range []string{"continue", "recover", "restart", "abort"} {
		s.AddCommand(name, forwardCommand(name, f, Continue))
	}

	fmt.Fprintf(iop, "%s", PROMPT)
	return s
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, USAGE)
		flag.PrintDefaults()
	}

	flag.Parse()
	if *showvers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	pt, err := loadPtable(args[0], *onePtab)
	if err != nil {
		log.Fatalf("Cannot read parameter table: %v", err)
	}

	var f io.WriteCloser
	if *fwdFile != "" {
		f, err := os.Create(*fwdFile)
		if err != nil {
			log.Printf("Cannot open %s: %v", *fwdFile, err)
		} else {
			defer f.Close()
		}
	}

	s := createShell(&pt, f)
	if *timeOut != 0 {
		ctx, cancel := context.WithTimeout(context.Background(), *timeOut)
		defer cancel()
		err = s.Run(ctx)
	} else {
		err = s.Run(context.Background())
	}

	if err != nil {
		log.Println(err)
	}

	os.Exit(exitCode)
}
