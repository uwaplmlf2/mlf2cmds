module bitbucket.org/uwaplmlf2/mlf2cmds

require (
	bitbucket.org/uwaplmlf2/go-mlf2 v0.9.16
	bitbucket.org/uwaplmlf2/go-shell v0.5.0
	github.com/google/shlex v0.0.0-20181106134648-c34317bd91bf
	github.com/pkg/errors v0.8.1
)

go 1.13
